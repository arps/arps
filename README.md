ARPS: Framework to create and deploy Multi-Agents
=====================

ARPS is an acronym that comes from the simplest definition of a
software agent is:

* Autonomous: the ability to run without human interaction

* Reflexive: the ability to act upon events

* Proactive: the ability to make its own decisions

* Social: ability to interact with other agents to better achieve its
  objectives

# Description

ARPS is a framework to create Multi-Agent System (MAS). It provides a
Discrete Event Simulation component to evaluate the agents'
behaviour in a controlled environment before deployment in a actual environment.

This framework does not impose any agent model for implementation.

# Developing

See [CONTRIBUTING](CONTRIBUTING.md)

# Example

See how this [example](https://gitlab.com/arps/arps_example_0) uses the ARPS framework
to monitor computational resources

-----------------

* See [wiki](https://gitlab.com/arps/arps/wikis/home) for more information
