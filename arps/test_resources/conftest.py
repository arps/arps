import asyncio

import pytest_asyncio

from arps.core.clock import simulator_clock_factory
from arps.core.real.raw_communication_layer import RawCommunicationLayer
from arps.core.real.rest_communication_layer import RESTCommunicationLayer
from arps.test_resources.fake_agents_directory_helper import FakeAgentsDirectoryHelper


@pytest_asyncio.fixture(params=(RawCommunicationLayer, RESTCommunicationLayer))
async def real_env_components(request):
    agents_directory_helper = FakeAgentsDirectoryHelper()
    clock = simulator_clock_factory()

    clock_task = asyncio.create_task(clock.run())

    yield clock, agents_directory_helper, request.param

    clock_task.cancel()

    await clock_task
